// IMPORT
const express = require('express')
const app = express();
app.use(express.json()); // support json encoded bodies
app.use(express.urlencoded({extended:true})) // support encoded bodies
app.use(express.static("uploads"));

const oauth20 = require('./Google_Oauth_2.0');
// Credentials
// const GOOGLE_CLIENT_ID = `968361167831-0bakrhg63n7dqef6ov11am0kdd6l28q2.apps.googleusercontent.com`;
// const GOOGLE_CLIENT_SECRET = `rls69Ymzo0m-J46bktjuD8hz`;
// const CLIENT_CALLBACK = `http://localhost:3000/oauth2callback`;
 
const router = {
    user: require('./Controllers/UserController'),
    company: require('./Controllers/CompanyController'),
    job: require('./Controllers/JobController'),
    sub: require('./Controllers/SubscriptionController'),
    app: require('./Controllers/ApplicationController')
};
// ========================================================================= //

// ROUTE >> jangan ada /api
app.get('/oauth2callback', (req, res) => {
    return res.send("Success Login")
});

app.use('/user', router.user);
app.use('/company', router.company);
app.use('/job', router.job);
app.use('/subscription', router.sub);
app.use('/application', router.app);
// ========================================================================= //


// PORT LISTENING
const port = 3000;
app.listen(port, () => {
    console.log(`Listening to port ${port}`)
});
// ========================================================================= //