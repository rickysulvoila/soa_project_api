const jwt = require('jsonwebtoken');
const secret = "50APr0j3ct";//gaby

const jwtSign = (data) => { 
    return jwt.sign(
        data, 
        secret,
        {
            expiresIn:"1h"
        }
    ); 
}

const verifyToken = (token) => {
    let user = {};

    if (!token) { 
        return {
            status: 401,
            message: 'Unauthorized'
        };
    }

    try { 
        user = jwt.verify(token, secret); 
    }
    catch(err){
        return {
            status: 401,
            message: 'Invalid Token'
        };
    }

    // console.log("Date now : " + (new Date().getTime()/1000))
    // console.log("User IAT : " + user.iat);
    // console.log("User EXP : " + user.exp);

    //if ((new Date().getTime()/1000) >= user.exp) { 
        //if ((new Date().getTime()/1000)-user.iat >= 1800) {
    //    return {
    //       status: 400,
    //       message: 'Token expired'
    //   };
    //}

    return {
        status: 200,
        message: 'Authentication successful',
        data: user
    };
}

module.exports = {
    'makeToken': jwtSign,
    'verifyToken': verifyToken
}