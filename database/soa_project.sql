-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 05 Jul 2021 pada 05.59
-- Versi server: 10.4.14-MariaDB
-- Versi PHP: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `soa_project`
--
CREATE DATABASE IF NOT EXISTS `soa_project` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `soa_project`;

-- --------------------------------------------------------

--
-- Struktur dari tabel `applications`
--

DROP TABLE IF EXISTS `applications`;
CREATE TABLE `applications` (
  `id_app` int(11) NOT NULL,
  `fk_jobs` int(11) NOT NULL,
  `fk_pelamar` int(11) NOT NULL,
  `file_cv` varchar(50) NOT NULL,
  `status_app` int(1) NOT NULL COMMENT '0(ditolak)/1(diterima)',
  `submitted_at` datetime DEFAULT NULL,
  `event_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `applications`
--

INSERT INTO `applications` (`id_app`, `fk_jobs`, `fk_pelamar`, `file_cv`, `status_app`, `submitted_at`, `event_id`) VALUES
(2, 2, 2, './public/applications/APPLICATION_2.pdf', 1, '2021-05-18 00:00:00', 'm9moo4rajdh08p62egr41m2634'),
(4, 3, 2, '', 1, '2021-05-19 23:25:12', 'm9moo4rajdh08p62egr41m2634'),
(5, 3, 1, './public/applications/APPLICATION_5.pdf', 1, '2021-05-24 00:00:00', 'm9moo4rajdh08p62egr41m2634'),
(6, 7, 1, './public/applications/APPLICATION_6.pdf', 1, '2021-05-24 00:00:00', 'm9moo4rajdh08p62egr41m2634'),
(7, 6, 1, './public/applications/APPLICATION_7.pdf', 0, '2021-06-17 00:00:00', NULL),
(8, 4, 1, './public/applications/APPLICATION_8.pdf', -1, '2021-06-17 00:00:00', NULL),
(9, 5, 1, './public/applications/APPLICATION_9.pdf', -1, '2021-06-17 00:00:00', NULL),
(10, 5, 3, './public/applications/APPLICATION_10.pdf', 0, '2021-06-17 00:00:00', NULL),
(11, 4, 3, './public/applications/APPLICATION_11.pdf', -1, '2021-06-17 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `bidang`
--

DROP TABLE IF EXISTS `bidang`;
CREATE TABLE `bidang` (
  `id_bidang` int(11) NOT NULL,
  `nama_bidang` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `bidang`
--

INSERT INTO `bidang` (`id_bidang`, `nama_bidang`) VALUES
(1, 'Writer'),
(2, 'Programmer'),
(3, 'Doctor'),
(4, 'Actor'),
(5, 'Accountant'),
(6, 'Chef'),
(7, 'Fashion Designer'),
(8, 'Lawyer'),
(9, 'Architect'),
(10, 'Journalist'),
(11, 'Lecturer'),
(12, 'Librarian'),
(13, 'Electrician'),
(14, 'Hairdresser'),
(15, 'Nurse'),
(16, 'Pilot'),
(17, 'Politician'),
(18, 'Waiter'),
(19, 'Translator'),
(20, 'Secretary'),
(21, 'Event Organizer'),
(22, 'Receptionist'),
(23, 'Travel Agent'),
(24, 'Tailor'),
(25, 'Teacher'),
(26, 'Real Estate Agent'),
(27, 'Shop Assistant'),
(28, 'Pharmacist'),
(29, 'Taxi Driver'),
(30, 'Judge'),
(31, 'UI / UX Designer'),
(32, 'Enterpreneur'),
(33, 'Tour Guide'),
(34, 'Api Developer'),
(35, 'Administration'),
(36, 'Salesman'),
(37, 'Executive Assistant'),
(38, 'IT Consultant'),
(39, 'Education Consultant'),
(40, 'Marketing Executive'),
(41, 'Sales Marketing'),
(42, 'Manager'),
(43, 'Packaging Designer'),
(44, 'Customer Service'),
(45, 'Model');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jobs`
--

DROP TABLE IF EXISTS `jobs`;
CREATE TABLE `jobs` (
  `id_jobs` int(11) NOT NULL,
  `fk_perusahaan` int(11) NOT NULL,
  `fk_bidang` int(11) NOT NULL,
  `min_gaji` int(11) NOT NULL,
  `maks_gaji` int(11) NOT NULL,
  `tanggal_posting` date NOT NULL,
  `jenis_pekerjaan` varchar(10) NOT NULL COMMENT 'Full-time/Part-time',
  `kualifikasi` varchar(20) NOT NULL,
  `spesialisasi` varchar(20) NOT NULL,
  `status_job` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `jobs`
--

INSERT INTO `jobs` (`id_jobs`, `fk_perusahaan`, `fk_bidang`, `min_gaji`, `maks_gaji`, `tanggal_posting`, `jenis_pekerjaan`, `kualifikasi`, `spesialisasi`, `status_job`) VALUES
(1, 1, 43, 0, 0, '2021-05-18', 'Full-time', 'h', 'h', 0),
(2, 2, 5, 2000000, 3000000, '2021-04-19', 'Part-time', 'IPK 2.5', 'Pembukuan', 0),
(3, 1, 20, 1000099, 1200000, '2021-05-18', 'Full-time', 'h', 'h', 1),
(4, 2, 44, 14000000, 25500000, '2021-05-18', 'Part-time', 'IPK 3', 'Good Talker', 1),
(5, 2, 13, 5000500, 11000000, '2021-05-19', 'Part-time', 'Study Electro', 'Machine', 1),
(6, 3, 45, 5000000, 7000000, '2021-05-19', 'Part-time', 'Height 170cm', 'Fashion', 1),
(7, 3, 14, 2500000, 8000000, '2021-04-24', 'Part-time', 'Study in Hair Stylin', 'Good Speaker', 1),
(8, 4, 6, 2500000, 15000000, '2021-05-29', 'Full-time', 'Magang min 3 kali', 'Masakan Seafood', 1),
(9, 2, 20, 1100000, 3400000, '2021-06-05', 'Part-time', 'Umur 20 tahun keatas', 'Tanggap', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pelamar`
--

DROP TABLE IF EXISTS `pelamar`;
CREATE TABLE `pelamar` (
  `id_pelamar` int(11) NOT NULL,
  `fk_user` int(11) NOT NULL,
  `nama_depan` varchar(20) DEFAULT NULL,
  `nama_belakang` varchar(20) DEFAULT NULL,
  `alamat_pelamar` varchar(50) DEFAULT NULL,
  `bidang_studi` varchar(50) DEFAULT NULL COMMENT 'S1/D3'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `pelamar`
--

INSERT INTO `pelamar` (`id_pelamar`, `fk_user`, `nama_depan`, `nama_belakang`, `alamat_pelamar`, `bidang_studi`) VALUES
(1, 2, 'Gabriel', 'Angelin', 'jalan semolowaru gang 29', 'Teknik Industri'),
(2, 6, 'ricky', 'sutanto', 'Wiyung 8 No 110', 'Teknik Informatika'),
(3, 1, 'angelica', 'fredana', 'jalan nginden jaya barat 20', 'Desain Interior'),
(5, 7, 'frederick', 'ricky', 'Manyar jaya B8-912', 'Perhotelan'),
(6, 9, 'Christo', 'Wijaya', 'Tanah Abang no 51', 'Desain Produk'),
(10, 8, 'susanto', 'hendra', 'Manyar Tirtoasri 204', 'Perhotelan'),
(12, 13, 'Delvina', 'Sadria', 'jalan menur pumpungan 39', 'Arsitektur');

-- --------------------------------------------------------

--
-- Struktur dari tabel `perusahaan`
--

DROP TABLE IF EXISTS `perusahaan`;
CREATE TABLE `perusahaan` (
  `id_perusahaan` int(11) NOT NULL,
  `fk_user` int(11) NOT NULL,
  `nama_perusahaan` varchar(30) NOT NULL,
  `email_perusahaan` varchar(30) NOT NULL,
  `alamat_perusahaan` varchar(50) NOT NULL,
  `kota` varchar(20) NOT NULL,
  `notelp_perusahaan` varchar(12) NOT NULL,
  `status_perusahaan` int(1) NOT NULL COMMENT '0/1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `perusahaan`
--

INSERT INTO `perusahaan` (`id_perusahaan`, `fk_user`, `nama_perusahaan`, `email_perusahaan`, `alamat_perusahaan`, `kota`, `notelp_perusahaan`, `status_perusahaan`) VALUES
(1, 4, 'PT sambel ABC', 'vg4z@gmial.com', 'Kali raya sari', 'XYZ', '08945515754', 0),
(2, 5, 'PT Nusa Dua', 'nusa2network@gmail.com', 'Klampis Raya VII no 20', 'Surabaya', '086133458915', 1),
(3, 5, 'Garuda Wallet', 'garudawallet@gmail.com', 'Galaxy Jaya Indah', 'Surabaya', '0314784565', 1),
(4, 4, 'Sekotel Bali', 'sekotelbali@yahoo.com', 'Dewata Raya J-20', 'Bali', '0983242353', 1),
(5, 5, 'PT DFC Softwarehouse', 'softwarehouse_dfc@gmail.com', 'Kali Raya Sari', 'XYS', '08746132589', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `subscriptions`
--

DROP TABLE IF EXISTS `subscriptions`;
CREATE TABLE `subscriptions` (
  `id_subs` int(11) NOT NULL,
  `fk_pelamar` int(11) NOT NULL,
  `bukti_transfer` varchar(50) NOT NULL,
  `status_subs` int(1) NOT NULL COMMENT '0/1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `subscriptions`
--

INSERT INTO `subscriptions` (`id_subs`, `fk_pelamar`, `bukti_transfer`, `status_subs`) VALUES
(1, 1, './public/uploads/BUKTI_TRANSFER_2.png', 1),
(2, 3, './public/uploads/BUKTI_TRANSFER_1.png', 1),
(3, 2, './public/uploads/BUKTI_TRANSFER_6.jpg', -1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id_user` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `email_user` varchar(30) NOT NULL,
  `api_key` varchar(20) NOT NULL,
  `password` varchar(30) NOT NULL,
  `nama_user` varchar(50) NOT NULL,
  `alamat_user` varchar(50) NOT NULL,
  `notelp_user` varchar(12) NOT NULL,
  `role` varchar(1) NOT NULL COMMENT 'G/N/P/A/C'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id_user`, `username`, `email_user`, `api_key`, `password`, `nama_user`, `alamat_user`, `notelp_user`, `role`) VALUES
(1, 'angelicafreda', 'angelica@stts.edu', 'KEyDVwhpzFj10KMVNzx6', 'fredalica', 'angelica fredana', 'jalan nginden jaya barat 20', '082342351', 'P'),
(2, 'gabrielleakho', 'gabrielle1@mhs.stts.edu', 'ROypCaMlaxzOhN2iqbJw', 'copypasta', 'Gabriel Angelin', 'jalan semolowaru gang 29', '0982348923', 'P'),
(3, 'fredalia', 'freda@yahoo.com', 'cLJpguql8cqeb3ltioUv', 'baratfreda', 'fredalia gabriella', 'jl nit A1-4', '082342351134', 'A'),
(4, 'vanguard4z', 'anthony1@mhs.stts.edu', 'IUQBExCFNpFUhaeqPV3N', 'prototypezero', 'anthony az', 'Mulyosari 108', '089677661550', 'C'),
(5, 'arcnemesis', 'arcnemes@gmail.com', '8NvWBny0XjcySq6ie9H6', '000111', 'del videl', 'Kenjeran 8 No 10', '089544617794', 'C'),
(6, 'rickysts11', 'ricky1@gmail.com', 'BdIPWPikh2YXSVqRhqCi', '221100', 'ricky sutanto', 'Wiyung 8 No 110', '082374862315', 'N'),
(7, 'fredricky00', 'fred@gmail.com', 'cvI6PmMbzq0I5UA5w60M', 'fredfred', 'frederick ricky', 'Manyar jaya B8-912', '082384723524', 'N'),
(8, 'susantodra', 'hendra@yahoo.com', 'b0trZam4EevnGSwSaMs9', 'hendralee33', 'susanto hendra kusuma', 'Manyar Tirtoasri 204', '091412451212', 'N'),
(12, 'Deriandi', 'cwirandi@gmail.com', 'ufig4OLO1p5AG39t7i8Q', 'LRBYXA', 'Christo Wijaya', 'Tanah Abang no 51', '089371098312', 'N'),
(13, 'delvidara', 'delvidara@gmail.com', 'sQhZGvvM3K5yuPPm9AX8', 'daradelvi', 'Delvina Sadria', 'jalan menur pumpungan 39', '0821482581', 'N');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `applications`
--
ALTER TABLE `applications`
  ADD PRIMARY KEY (`id_app`);

--
-- Indeks untuk tabel `bidang`
--
ALTER TABLE `bidang`
  ADD PRIMARY KEY (`id_bidang`);

--
-- Indeks untuk tabel `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id_jobs`);

--
-- Indeks untuk tabel `pelamar`
--
ALTER TABLE `pelamar`
  ADD PRIMARY KEY (`id_pelamar`);

--
-- Indeks untuk tabel `perusahaan`
--
ALTER TABLE `perusahaan`
  ADD PRIMARY KEY (`id_perusahaan`);

--
-- Indeks untuk tabel `subscriptions`
--
ALTER TABLE `subscriptions`
  ADD PRIMARY KEY (`id_subs`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `applications`
--
ALTER TABLE `applications`
  MODIFY `id_app` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `bidang`
--
ALTER TABLE `bidang`
  MODIFY `id_bidang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT untuk tabel `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id_jobs` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `pelamar`
--
ALTER TABLE `pelamar`
  MODIFY `id_pelamar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `perusahaan`
--
ALTER TABLE `perusahaan`
  MODIFY `id_perusahaan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `subscriptions`
--
ALTER TABLE `subscriptions`
  MODIFY `id_subs` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
