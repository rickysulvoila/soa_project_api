const express = require('express');
const router = express.Router();
const db = require('../connection');
const jwt = require('../Authentication')

//tabel users
/*
 `id_user` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `email_user` varchar(30) NOT NULL,
  `api_key` varchar(20) NOT NULL,
  `password` varchar(30) NOT NULL,
  `alamat_user` varchar(50) NOT NULL,
  `notelp_user` varchar(12) NOT NULL,
  `role` varchar(1) NOT NULL COMMENT 'G/P/A/C'
*/

//catatan gaby:
//find "gaby"
//response nya diperhatikan (codenya sesuaikan, pake bahasa indo semua)
//kasih "done" kalo selesai cek dan coding

// Coba email (Ricky)
var nodemailer = require('nodemailer');
var mail = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: 'dothetask.service@gmail.com',
      pass: 'kovjwapavvlpqyvz'
    }
});

function getRandomString(length) {
    var randomChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var result = '';
    for ( var i = 0; i < length; i++ ) {
        result += randomChars.charAt(Math.floor(Math.random() * randomChars.length));
    }
    return result;
}

//common query
function getQuery(param,mode){
    var query = '';
    if(mode==1){//with username
        query = `select * from users where username = '${param}'`;
    }
    else if(mode==2){//with id
        query = `select * from users where id_user = '${param}'`;
    }
    return query;
}

//done
router.post('/register', async(req,res) => {
    let role = req.body.role_user; //role yg ditentukan: pemilik perusahaan(C), pelamar(G/P), admin(A)
    
    //cek input user
    if (req.body.nama_depan == undefined || req.body.nama_depan == "") return res.status(400).send("Nama Depan Belum Diinputkan"); 
    else if (req.body.nama_belakang == undefined || req.body.nama_belakang == "") return res.status(400).send("Nama Belakang Belum Diinputkan"); 
    else if (req.body.email == undefined || req.body.email == "") return res.status(400).send("Email Belum Diinputkan"); 
    else if (req.body.username == undefined || req.body.username == "") return res.status(400).send("Username Belum Diinputkan"); 
    else if (req.body.password == undefined || req.body.password == "") return res.status(400).send("Password Belum Diinputkan"); 
    else if (req.body.confirm_password == undefined || req.body.confirm_password == "") return res.status(400).send("Confirm Password Belum Diinputkan"); 
    else if (req.body.alamat == undefined || req.body.alamat == "") return res.status(400).send("Alamat Belum Diinputkan"); 
    else if (req.body.nomor_telp == undefined || req.body.nomor_telp == "") return res.status(400).send("No Telepon Belum Diinputkan"); 
    else if (role == undefined || role == "") return res.status(400).send("Role User Belum Diinputkan"); 
    
    //cek format email (@ dan .com)
    let lengkap = 0;
    for (let i = 0; i < req.body.email.length; i++) {
        if(req.body.email.charAt(i)=="@"){
            lengkap++;
        }
        let dotcom = req.body.email.charAt(i)+req.body.email.charAt(i+1)
            +req.body.email.charAt(i+2)+req.body.email.charAt(i+3);
        //console.log(dotcom);
        if(dotcom==".com" || dotcom==".edu"){
            lengkap++;
        }
    }
    //console.log(lengkap);
    //console.log(req.body.nomor_telp != parseInt(req.body.nomor_telp) ? true : false );
    if(lengkap<2) return res.status(400).send('Email anda tidak valid!');
    
    //cek isi input
    if(req.body.username.length < 8){
        return res.status(400).send("Username harus memiliki panjang di atas 8!");
    }
    else if(req.body.password != req.body.confirm_password){
        return res.status(400).send("Password dan Confirm Password tidak sama!");
    }
    else if(req.body.nomor_telp != parseInt(req.body.nomor_telp)){
        return res.status(400).send("Nomor telepon harus berupa angka!");
    }
    else if(role.toUpperCase()!="G" && role.toUpperCase()!="C" && role.toUpperCase()!="A" && role.toUpperCase()!="N"){
        return res.status(401).send("Role User yang tersedia: G / C / A / N!");
    }
    //declare awal
    let nama        = req.body.nama_depan+" "+req.body.nama_belakang;
    let username    = req.body.username;
    let email       = req.body.email;
    let password    = req.body.password;
    let alamat      = req.body.alamat;
    let nohp        = req.body.nomor_telp;
    let temprole    = role.toUpperCase();//case insensitive

    //tampilan role
    let showrole="";
    if(temprole=="G") showrole="Guest";//guest
    else if(temprole=="N") showrole="Pelamar";//pelamar status newbie
    else if(temprole=="C") showrole="Pemilik Perusahaan";//pemilik perusahaan 
    else if(temprole=="A") showrole="Admin";

    //query select
    let conn = await db.getConn();
    var q1 = getQuery(username,1);
    let selected = await db.executeQuery(conn, q1);
    conn.release();
    if(selected.length==0){
        //query insert
        let api_key=getRandomString(20); //lenght nyesuaikan sm kalian
        conn = await db.getConn();
        var q2=`insert into users values ('', '${username}', '${email}','${api_key}','${password}','${nama}','${alamat}','${nohp}','${temprole}')`;
        let inserted = await db.executeQuery(conn, q2);
        conn.release();
        if (inserted.affectedRows === 0) {
            return res.status(500).send('Internal server error!');
        }

        //if pelamar
        if(temprole=="N" && req.body.bidang_studi!=""){
            conn = await db.getConn();
            var q3=getQuery(username,1);
            newUser = await db.executeQuery(conn, q3);
            conn.release();
            conn = await db.getConn();
            var q4=`insert into pelamar values ('', '${newUser[0].id_user}', '${req.body.nama_depan}',
                '${req.body.nama_belakang}','${req.body.alamat}','${req.body.bidang_studi}')`;
            newPelamar = await db.executeQuery(conn, q4);
            conn.release();
            if (newPelamar.affectedRows === 0) {
                return res.status(500).send('Internal server error!');
            }
        }

        //tampilan user
        let newData={
            "username":username,
            "nama_user":nama,
            "email_user": email,
            "alamat_user":alamat,
            "nomor_telepon_user":nohp,
            "api_key":api_key,
            "role_user":showrole
        }
        return res.status(201).json({
            register_user: newData
        });
    }
    else return res.status(400).send("User telah terdaftar dalam database!");
});

//done
router.post('/login', async(req,res) => { 
    //cek input
    if (req.body.username == undefined || req.body.username == "") return res.status(400).send("Username Belum Diinputkan"); 
    else if (req.body.password == undefined || req.body.password == "") return res.status(400).send("Password Belum Diinputkan");
    else{
        let username    = req.body.username;
        let password    = req.body.password;
        
        //query select
        let conn = await db.getConn();
        var query=getQuery(username,1);
        let login = await db.executeQuery(conn, query);
        conn.release();
        if (login.length == 0) {
            return res.status(404).send('User tidak terdaftar dalam database!');
        }
        else if(login[0].password!=password){
            return res.status(400).send('Password anda salah!');
        }
        //buat token
        let newData={
            "id": login[0].id_user,
            "api_key": login[0].api_key,
            "role" : login[0].role
        }
        var token=jwt.makeToken(newData);
        return res.status(200).json({
            message: login[0].nama_user+" ("+login[0].role+") berhak login selama 1 jam",
            token: token
        })
    }
}); 

//update profile done
router.put('/profile', async(req,res) => {
    //cek token
    var verifying = jwt.verifyToken(req.header('x-auth-token'));
    if(verifying.status!=200){
        return res.status(verifying.status).send(verifying.message);
    }
    else if(verifying.data.role=="N" || verifying.data.role=="P"){
        return res.status(401).send("Update profile hanya untuk Guest / Pemilik perusahaan / Admin");
    }

    //tampilan role
    let showrole="";
    if(verifying.data.role=="G") showrole="Guest";//guest
    else if(verifying.data.role=="C") showrole="Pemilik Perusahaan";//pemilik perusahaan 
    else if(verifying.data.role=="A") showrole="Admin";

    //cek input
    if (req.body.nama_user == undefined || req.body.nama_user == "") return res.status(400).send("Nama User Belum Diinputkan"); 
    else if (req.body.alamat == undefined || req.body.alamat == "") return res.status(400).send("Alamat Belum Diinputkan"); 
    else if (req.body.nomor_telp == undefined || req.body.nomor_telp == "") return res.status(400).send("No Telepon Belum Diinputkan"); 
    
    //cek isi input
    if(req.body.nomor_telp != parseInt(req.body.nomor_telp)){
        return res.status(400).send("Nomor telepon harus berupa angka!");
    }
    
    let nama        = req.body.nama_user;
    let alamat      = req.body.alamat;
    let nohp        = req.body.nomor_telp;

    //query select sesuai token
    let conn = await db.getConn();
    var q1=getQuery(verifying.data.id,2);
    let selected = await db.executeQuery(conn, q1);
    conn.release();
    console.log(selected);
    if(selected.length==1){
        //query update
        conn = await db.getConn();
        var q2=`update users set nama_user='${nama}',alamat_user='${alamat}',notelp_user='${nohp}' where username='${selected[0].username}'`;
        let updated = await db.executeQuery(conn, q2);
        conn.release();
        if (updated.affectedRows === 0) {
            return res.status(500).send('Internal server error!');
        }
        return res.status(200).json({
            username_login :selected[0].username,
            role_user:showrole,
            previous_data: {
                "nama_user":selected[0].nama_user,
                "alamat_user":selected[0].alamat_user,
                "nomor_telepon_user":selected[0].notelp_user
            },
            updated_data: {
                "nama_user":nama,
                "alamat_user":alamat,
                "nomor_telepon_user":nohp
            }
        });
    }
    else return res.status(400).send("User tidak terdaftar dalam database!");
});

//upgrade G jadi N done
router.post('/pelamar/register', async(req,res) => {
    //cek token
    var verifying = jwt.verifyToken(req.header('x-auth-token'));
    if(verifying.status!=200){
        return res.status(verifying.status).send(verifying.message);
    }
    else if(verifying.data.role!="G"){
        return res.status(401).send("Register sebagai pelamar hanya untuk Guest");
    }

    //cek input
    if (req.body.bidang_studi == undefined || req.body.bidang_studi == "") return res.status(400).send("Bidang Studi Belum Diinputkan"); 
    
    //query select
    let conn = await db.getConn();
    var q1 = getQuery(verifying.data.id,2);
    let selected = await db.executeQuery(conn, q1);
    conn.release();
    if(selected.length==1){
        //query insert pelamar
        let nama = selected[0].nama_user.split(" ");
        console.log(nama[0]+" 0 "+nama[1]);
        let nama_depan = nama[0];
        let nama_belakang = nama[1];
        conn = await db.getConn();
        var q2=`insert into pelamar values ('', '${verifying.data.id}', '${nama_depan}',
            '${nama_belakang}','${selected[0].alamat_user}','${req.body.bidang_studi}')`;
        let newPelamar = await db.executeQuery(conn, q2);
        conn.release();
        if (newPelamar.affectedRows === 0) {
            return res.status(500).send('Internal server error!');
        }
        //query update users
        conn = await db.getConn();
        var q3=`update users set role='N' where id_user='${verifying.data.id}'`;
        let updated = await db.executeQuery(conn, q3);
        conn.release();
        if (updated.affectedRows === 0) {
            return res.status(500).send('Internal server error!');
        }
        return res.status(201).json({
            register_pelamar: {
                "username":selected[0].username,
                "nama_depan":nama_depan,
                "nama_belakang":nama_belakang,
                "alamat_pelamar":selected[0].alamat_user,
                bidang_studi:req.body.bidang_studi,
                role_user: "Pelamar"
            }
        });
    }
    else return res.status(400).send("User belum terdaftar dalam database!");
});

//update profile pelamar done
router.put('/pelamar/profile', async(req,res) => {
    //cek token
    var verifying = jwt.verifyToken(req.header('x-auth-token'));
    if(verifying.status!=200){
        return res.status(verifying.status).send(verifying.message);
    }
    if(verifying.data.role!="N" && verifying.data.role!="P"){
        return res.status(401).send("User yang bisa menggunakan hanya Pelamar atau Pelamar dengan status Premium");
    }

    //tampilan role
    let showrole="";
    if(verifying.data.role=="N") showrole="Pelamar";//pelamar newbie
    else if(verifying.data.role=="P") showrole="Pelamar Premium";//pelamar status premium

    //cek input
    if (req.body.nama_depan == undefined || req.body.nama_depan == "") return res.status(400).send("Nama Depan Belum Diinputkan"); 
    else if (req.body.nama_belakang == undefined || req.body.nama_belakang == "") return res.status(400).send("Nama Belakang Belum Diinputkan"); 
    else if (req.body.alamat == undefined || req.body.alamat == "") return res.status(400).send("Alamat Pelamar Belum Diinputkan"); 
    else if (req.body.bidang_studi == undefined || req.body.bidang_studi == "") return res.status(400).send("Bidang Studi Belum Diinputkan"); 
    
    //query select user sesuai token
    let conn = await db.getConn();
    var q1 = getQuery(verifying.data.id,2);
    let selectUser = await db.executeQuery(conn, q1);
    conn.release();
    console.log(selectUser);
    if(selectUser.length==0) return res.status(400).send("User tidak terdaftar dalam database!");
    
    //query select pelamar
    conn = await db.getConn();
    var query=`select * from pelamar where fk_user='${verifying.data.id}'`;
    let selected = await db.executeQuery(conn, query);
    conn.release();
    console.log(selected);
    if(selected.length==1){
        //query update user
        conn = await db.getConn();
        var q2=`update users set nama_user='${req.body.nama_depan+' '+req.body.nama_belakang}',
            alamat_user='${req.body.alamat}' where id_user='${verifying.data.id}'`;
        let updUser = await db.executeQuery(conn, q2);
        conn.release();
        if (updUser.affectedRows === 0) {
            return res.status(500).send('Internal server error!');
        }
        //query update pelamar
        conn = await db.getConn();
        var q2=`update pelamar set nama_depan='${req.body.nama_depan}', nama_belakang='${req.body.nama_belakang}',
            alamat_pelamar='${req.body.alamat}',bidang_studi='${req.body.bidang_studi}' 
            where fk_user='${verifying.data.id}'`;
        let updated = await db.executeQuery(conn, q2);
        conn.release();
        if (updated.affectedRows === 0) {
            return res.status(500).send('Internal server error!');
        }
        return res.status(200).json({
            username_login :selectUser[0].username,
            role_user:showrole,
            previous_data: {
                "nama_depan":selected[0].nama_depan,
                "nama_belakang":selected[0].nama_belakang,
                "alamat_pelamar":selected[0].alamat_pelamar,
                "bidang_studi":selected[0].bidang_studi
            },
            updated_data: {
                "nama_depan":req.body.nama_depan,
                "nama_belakang":req.body.nama_belakang,
                "alamat_pelamar":req.body.alamat,
                "bidang_studi":req.body.bidang_studi
            }
        });
    }
    else return res.status(400).send("User tidak terdaftar sebagai pelamar atau pelamar premium!");
});

// get user/profile yg login done
router.get('/profile', async(req,res) => { 
    //cek token
    var verifying = jwt.verifyToken(req.header('x-auth-token'));
    if(verifying.status!=200){
        return res.status(verifying.status).send(verifying.message);
    }

    var id=verifying.data.id;
    //buat tunjukan role
    let showrole="";
    if(verifying.data.role=="G") showrole="Guest";//guest
    else if(verifying.data.role=="N") showrole="Pelamar";//pelamar status newbie
    else if(verifying.data.role=="P") showrole="Pelamar Premium";//pelamar status premium
    else if(verifying.data.role=="C") showrole="Pemilik Perusahaan";//pemilik perusahaan 
    else if(verifying.data.role=="A") showrole="Admin";
    
    let conn = await db.getConn();
    var q1=getQuery(id,2);
    let selected = await db.executeQuery(conn, q1);
    conn.release();
    console.log(selected);
    if(selected.length==1){
        //if pelamar (gaby)
        if(showrole=="Pelamar" || showrole=="Pelamar Premium"){
            conn = await db.getConn();
            var queryPelamar=`select p.nama_depan as depan, p.nama_belakang as belakang,
                p.alamat_pelamar as alamat, p.bidang_studi as studi from users u join
                pelamar p on u.id_user=p.fk_user and u.id_user='${id}'`;
            let pelamar = await db.executeQuery(conn, queryPelamar);
            conn.release();
            console.log(pelamar);
            return res.status(200).json({
                user_profile:{
                    "username":selected[0].username,
                    "nama":selected[0].nama_user,
                    "email": selected[0].email_user,
                    "alamat":selected[0].alamat_user,
                    "nomor_telepon":selected[0].notelp_user,
                    "api_key":selected[0].api_key
                },
                pelamar_profile:{
                    status: showrole,
                    nama_depan: pelamar[0].depan,
                    nama_belakang: pelamar[0].belakang,
                    alamat_pelamar: pelamar[0].alamat,
                    bidang_studi: pelamar[0].studi
                }
            });
        }
        //selain pelamar
        return res.status(200).json({
            profile:{
                "username":selected[0].username,
                "nama_user":selected[0].nama_user,
                "email_user": selected[0].email_user,
                "alamat_user":selected[0].alamat_user,
                "nomor_telepon_user":selected[0].notelp_user,
                "api_key":selected[0].api_key,
                "role_user":showrole
            }
        });
    }
    else return res.status(404).send("User tidak terdaftar dalam database!");

});

//pengecekan user
router.get('/all', async(req,res) => { 
    let nama = "";
    if(req.query.nama) nama = req.query.nama; 

    let conn = await db.getConn();
    var q1= `select username, password, role from users`;
    if(nama) {
        let like = '%'+nama+'%';
        var tambahan= q1 + ` where nama_user like '${like}' or username like '${like}'`;
        q1= tambahan;
    }
    let selected = await db.executeQuery(conn, q1);
    conn.release();
    console.log(selected);
    if(selected.length>=1){
        
        return res.status(200).json({
            jumlah: selected.length,
            users:selected
        });
    }
    else return res.status(404).send("User tidak terdaftar dalam database!");

});

//bagian Ricky (yg email)
router.post('/resetPassword', async(req,res) =>{
    //alur gaby:
    //1 cek token (lihat update profile)
    //2 cek input (new_password, confirm_password) >> lihat update profile
    //3 query select dan update users sesuai token (lihat update profile)
    //4 tampilan response:
    /*
        res.status(200).send("Reset password berhasil")
    */
    var verifying = jwt.verifyToken(req.header('x-auth-token'));
    if(verifying.status!=200){
        return res.status(verifying.status).send(verifying.message);
    }
    var id=verifying.data.id;


    //antara user reset sendiri atau provider yg reset
    if (req.body.new_password == undefined || req.body.password == "") {return res.status(400).send("Password Belum Diinputkan"); }
    else if (req.body.confirm_password == undefined || req.body.confirm_password == "") {return res.status(400).send("Password Belum Diinputkan"); }
    else{
        var passbaru=req.body.new_password;
        var cpassbaru=req.body.confirm_password;

        //cek pass dan cpass sama atau ngga
        if(passbaru==cpassbaru){
            //select users
            let conn = await db.getConn();
            var q1=getQuery(verifying.data.id,2);
            let selected = await db.executeQuery(conn, q1);
            conn.release();
            console.log(selected);
            
            if(selected.length==1){
                //update pass users
                conn = await db.getConn();
                var q2=`update users set password='${passbaru}'where id_user='${id}'`;
                let updated=await db.executeQuery(conn, q2);
                conn.release();
                if (updated.affectedRows === 0) {
                    return res.status(500).send('Internal server error!');
                }
                //mark email
                return res.status(200).send("Reset password berhasil")
                
            }
            else return res.status(404).send("User tidak terdaftar dalam database!");
        }
        else{
            return res.status(400).send("Password dan Confirm Password tidak sama");
        }


        }
});

module.exports = router;