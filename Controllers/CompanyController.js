const express = require('express');
const router = express.Router();
const db = require('../connection');
const jwt = require('../Authentication')

/*
    CREATE TABLE `perusahaan` (
    `id_perusahaan` int(11) NOT NULL,
    `fk_user` int(11) NOT NULL,
    `nama_perusahaan` varchar(30) NOT NULL,
    `email_perusahaan` varchar(30) NOT NULL,
    `alamat_perusahaan` varchar(50) NOT NULL,
    `kota` varchar(20) NOT NULL,
    `notelp_perusahaan` varchar(12) NOT NULL,
    `status_perusahaan` int(1) NOT NULL COMMENT '0/1'
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
*/

//catatan gaby:
//find "gaby"
//response nya diperhatikan (codenya sesuaikan, pake bahasa indo semua)
//kasih "done" kalo selesai cek dan coding

// Coba email
var nodemailer = require('nodemailer');
const e = require('express');
var mail = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: 'dothetask.service@gmail.com',
      pass: 'kovjwapavvlpqyvz'
    }
});

//done
router.post('/register', async(req,res) => {
    //alur gaby: lihat register user karena mirip
    //1 cek token (lihat update profile user)
    //2 cek input, format email, dan isi input (pastikan nama perusahaan tidak sama dg perusahaan lain, role user = C, notelp harus angka)
    //3 query select users sesuai token (lihat update profile)
    //4 query insert perusahaan (fk_user dari id_user dari token, status default 1)
    //5 tampilan response:
    /*
        res.status(201).json({
            register_perusahaan:{
                "pemilik_perusahaan":nama_user,
                "nama_perusahaan":nama,
                "email_perusahaan": email,
                "alamat_perusahaan":alamat,
                "kota":kota
                "notelp_perusahaan":nohp
            }
        });
    */
    //cek token
    var verifying = jwt.verifyToken(req.header('x-auth-token'));
    if(verifying.status!=200){
        return res.status(verifying.status).send(verifying.message);
    }
    var id=verifying.data.id;
    if(verifying.data.role!="C"){
        return res.status(401).send("Register perusahaan wajib dari pemilik perusahaan(C)"); 
    }

    //cek input
    if (req.body.nama_perusahaan == undefined || req.body.nama_perusahaan == "") { return res.status(400).send("Nama Perusahaan Belum Diinputkan"); }
    else if (req.body.email_perusahaan == undefined || req.body.email_perusahaan == "") {return res.status(400).send("Email Perusahaan Belum Diinputkan"); }
    else if (req.body.alamat_perusahaan == undefined || req.body.alamat_perusahaan == "") {return res.status(400).send("Alamat Perusahaan Belum Diinputkan"); }
    else if (req.body.kota == undefined || req.body.kota == "") {return res.status(400).send("kota Belum Diinputkan"); }
    else if (req.body.notelp_perusahaan == undefined || req.body.notelp_perusahaan == "") {return res.status(400).send("No Telepon Perusahaan Belum Diinputkan"); }
    else{
        //deklarasi depan
        var nama_perusaahan=req.body.nama_perusahaan;
        var email_perusahaan=req.body.email_perusahaan;
        var alamat_perusahaan=req.body.alamat_perusahaan;
        var kota=req.body.kota;
        var notelp_perusahaan=req.body.notelp_perusahaan;


        //cek format email (@ dan .com)
        let lengkap = 0;
        for (let i = 0; i < email_perusahaan.length; i++) {
            if(email_perusahaan.charAt(i)=="@"){
                lengkap++;
            }
            let dotcom = email_perusahaan.charAt(i)+email_perusahaan.charAt(i+1)
                +email_perusahaan.charAt(i+2)+email_perusahaan.charAt(i+3);
            //console.log(dotcom);
            if(dotcom==".com" || dotcom==".edu"){
                lengkap++;
            }
        }
        //console.log(lengkap);
        //console.log(req.body.nomor_telp != parseInt(req.body.nomor_telp) ? true : false );
        if(lengkap<2) return res.status(400).send('Email anda tidak valid!');
        if(notelp_perusahaan!= parseInt(notelp_perusahaan)){
            return res.status(400).send("Nomor telepon perusahaan harus berupa angka!");
        }
        //nyari nama_user
        let conn = await db.getConn();
        var q1 = `select nama_user from users where id_user='${id}'`;
        let selected = await db.executeQuery(conn, q1);
        conn.release();
        if(selected.length==1){
            //nyimpen nama user
            var nama_owner=selected[0].nama_user;

            //check nama perusahaan apa kembar atau tidak
            conn = await db.getConn();
            var q2 = `select nama_perusahaan from perusahaan where nama_perusahaan='${nama_perusaahan}'`;
            let selected2 = await db.executeQuery(conn, q2);
            conn.release();
            if(selected2.length==1){
                return res.status(400).send("Nama perusahaan sudah pernah terdaftar !");
            }
            else{
                //insert company
                conn = await db.getConn();
                var q3 = `insert into perusahaan values('',${id},'${nama_perusaahan}','${email_perusahaan}','${alamat_perusahaan}','${kota}','${notelp_perusahaan}',1)`;;
                let inserted = await db.executeQuery(conn, q3);
                conn.release();
                
                if (inserted.affectedRows === 0) {
                    return res.status(500).send('Internal server error!');
                }

                return res.status(201).json({
                    register_perusahaan:{
                        "pemilik_perusahaan":nama_owner,
                        "nama_perusahaan":nama_perusaahan,
                        "email_perusahaan": email_perusahaan,
                        "alamat_perusahaan":alamat_perusahaan,
                        "kota":kota,
                        "notelp_perusahaan":notelp_perusahaan
                    }
                });
            }
        }
        else{
             return res.status(404).send("User tidak terdaftar dalam database!");
        }

    }
});

//done
router.get('/:nama_perusahaan', async(req,res) => {
    //cek token
    console.log(req.params.nama_perusahaan);
    var verifying = jwt.verifyToken(req.header('x-auth-token'));
    if(verifying.status!=200){
        return res.status(verifying.status).send(verifying.message);
    }
    var id=verifying.data.id;
    
    let nama = "";
    if(req.params.nama_perusahaan) nama = req.params.nama_perusahaan;
    console.log(nama+"--");
    
    if(verifying.data.role=="C"){
        //buat C
        //nyari nama_user
        let conn = await db.getConn();
        var q0 = `select nama_user from users where id_user='${id}'`;
        let selected0 = await db.executeQuery(conn, q0);
        conn.release();
        if(selected0.length==0){
            return res.status(404).send("User tidak terdaftar di database.");
        }
        //nyimpen nama user
        var nama_owner=selected0[0].nama_user;

        conn = await db.getConn();
        var q1=` select p.nama_perusahaan as nama,
            p.email_perusahaan as email, p.alamat_perusahaan as alamat,
            p.kota as kota, p.notelp_perusahaan as nomor_telepon 
            from users u, perusahaan p where p.fk_user = u.id_user 
            and p.status_perusahaan=1 and u.id_user='${id}'`;
        let like = '%'+nama+'%';
        var tambahan=q1+` and p.nama_perusahaan like '${like}'`;
        q1 = tambahan;
        
        let selected = await db.executeQuery(conn, q1);
        conn.release();
        console.log(selected);
        if(selected.length>0){
            return res.status(200).json({pemilik_perusahaan:nama_owner,perusahaan: selected});
        }
        else{
            return res.status(404).send("Perusahaan belum terdaftar dalam database");
        }
    }
    else{
        //buat G,P,A,N
        let conn = await db.getConn();
        var q1=` select u.nama_user as pemilik_perusahaan, p.nama_perusahaan as nama,
            p.email_perusahaan as email, p.alamat_perusahaan as alamat,
            p.kota as kota, p.notelp_perusahaan as nomor_telepon 
            from users u, perusahaan p where p.fk_user = u.id_user 
            and p.status_perusahaan=1`;
        let like = '%'+nama+'%';
        var tambahan=q1+` and p.nama_perusahaan like '${like}'`;
        q1 = tambahan;
        let selected = await db.executeQuery(conn, q1);
        conn.release();
        console.log(selected);
        if(selected.length>0){
            return res.status(200).json({perusahaan: selected});
        }
        else{
            return res.status(404).send("Perusahaan belum terdaftar dalam database");
        }
    }
});

//get tanpa nama_perusahaan (gaby) done
router.get('/', async(req,res) => {
    //cek token
    console.log(req.params.nama_perusahaan);
    var verifying = jwt.verifyToken(req.header('x-auth-token'));
    if(verifying.status!=200){
        return res.status(verifying.status).send(verifying.message);
    }
    var id=verifying.data.id;
    
    if(verifying.data.role=="C"){
        //buat C
        //nyari nama_user
        let conn = await db.getConn();
        var q0 = `select nama_user from users where id_user='${id}'`;
        let selected0 = await db.executeQuery(conn, q0);
        conn.release();
        if(selected0.length==0){
        return res.status(404).send("User tidak terdaftar di database.");
        }
        //nyimpen nama user
        var nama_owner=selected0[0].nama_user;

        conn = await db.getConn();
        var q1=` select p.nama_perusahaan as nama,
            p.email_perusahaan as email, p.alamat_perusahaan as alamat,
            p.kota as kota, p.notelp_perusahaan as nomor_telepon 
            from users u, perusahaan p where p.fk_user = u.id_user 
            and p.status_perusahaan=1 and u.id_user='${id}'`;
        let selected = await db.executeQuery(conn, q1);
        conn.release();
        console.log(selected);
        if(selected.length>0){
            return res.status(200).json({pemilik_perusahaan:nama_owner,perusahaan: selected});
        }
        else{
            return res.status(404).send("Perusahaan belum terdaftar dalam database");
        }
    }
    else{
        //buat G,P,A,N
        let conn = await db.getConn();
        var q1=` select u.nama_user as pemilik_perusahaan, p.nama_perusahaan as nama,
            p.email_perusahaan as email, p.alamat_perusahaan as alamat,
            p.kota as kota, p.notelp_perusahaan as nomor_telepon 
            from users u, perusahaan p where p.fk_user = u.id_user 
            and p.status_perusahaan=1`;
        let selected = await db.executeQuery(conn, q1);
        conn.release();
        console.log(selected);
        if(selected.length>0){
            return res.status(200).json({perusahaan: selected});
        }
        else{
            return res.status(404).send("Perusahaan belum terdaftar dalam database");
        }
    }
});

//done
router.put('/', async(req,res) => {
    //alur gaby: lihat update profile user karena mirip
    //1 cek token (lihat update profile user)
    //2 masukkan input perusahaan >> id_perusahaan, nama_perusahaan_baru,
        // email_perusahaan_baru, alamat_perusahaan_baru, 
        // kota_perusahaan_baru, notelp_perusahaan_baru
    //3 cek input, format email (lihat register user) dan isi input (role_user = C, notelp harus angka)
    //4 query select users sesuai token (lihat update profile user)
    //5 query select perusahaan (cek adanya perusahaan) dan update perusahaan (lihat update profile user)
    //6 tampilan response:
    /*
        res.status(201).json({
            username_pemilik :selected[0].username,
            nama_pemilik:selected[0].nama_user,
            previous_perusahaan: {
                "nama":perusahaan[0].nama_perusahaan,
                "alamat":perusahaan[0].alamat_perusahaan,
                "kota": perusahaan[0].kota,
                "nomor_telepon":perusahaan[0].notelp_perusahaan
            },
            updated_perusahaan: {
                "nama":nama,
                "alamat":alamat,
                "kota":kota,
                "nomor_telepon":nohp
            }
        });
    */
    
    console.log(req.params);
    //cek token
    var verifying = jwt.verifyToken(req.header('x-auth-token'));
    if(verifying.status!=200){
        return res.status(verifying.status).send(verifying.message);
    }
    var id=verifying.data.id;

    if(verifying.data.role!="C"){
        return res.status(401).send("Register perusahaan wajib dari pemilik perusahaan(C)"); 
    }
    //check input
    if (req.body.nama_perusahaan == undefined || req.body.nama_perusahaan == "") {return res.status(400).send("Nama Perusahaan Belum Diinputkan"); }
    else if (req.body.email_perusahaan_baru == undefined || req.body.email_perusahaan_baru == "") {return res.status(400).send("Email Perusahaan Baru Belum Diinputkan"); }
    else if (req.body.alamat_perusahaan_baru == undefined || req.body.alamat_perusahaan_baru == "") {return res.status(400).send("Alamat Perusahaan Baru  Belum Diinputkan"); }
    else if (req.body.kota_baru == undefined || req.body.kota_baru == "") {return res.status(400).send("kota Baru  Belum Diinputkan"); }
    else if (req.body.notelp_perusahaan_baru == undefined || req.body.notelp_perusahaan_baru == "") {return res.status(400).send("No Telepon Baru  Perusahaan Belum Diinputkan"); }
    else{
        var nama_perusaahan=req.body.nama_perusahaan;
        var email_perusahaan_baru=req.body.email_perusahaan_baru;
        var alamat_perusahaan_baru=req.body.alamat_perusahaan_baru;
        var kota_baru=req.body.kota_baru;
        var notelp_perusahaan_baru=req.body.notelp_perusahaan_baru;

        var email_perusahaan_lama="";
        var alamat_perusahaan_lama="";
        var kota_lama="";
        var notelp_perusahaan_lama="";

        //cek format email (@ dan .com)
        let lengkap = 0;
        for (let i = 0; i < email_perusahaan_baru.length; i++) {
            if(email_perusahaan_baru.charAt(i)=="@"){
                lengkap++;
            }
            let dotcom = email_perusahaan_baru.charAt(i)+email_perusahaan_baru.charAt(i+1)
                +email_perusahaan_baru.charAt(i+2)+email_perusahaan_baru.charAt(i+3);
            //console.log(dotcom);
            if(dotcom==".com" || dotcom==".edu"){
                lengkap++;
            }
        }
        //console.log(lengkap);
        //console.log(req.body.nomor_telp != parseInt(req.body.nomor_telp) ? true : false );
        if(lengkap<2) return res.status(400).send('Email anda tidak valid!');
        if(notelp_perusahaan_baru!= parseInt(notelp_perusahaan_baru)){
            return res.status(400).send("Nomor telepon perusahaan harus berupa angka!");
        }
        //select user
        let conn = await db.getConn();
        var q0 = `select * from users where id_user = '${id}'`;
        let selected0 = await db.executeQuery(conn, q0);
        conn.release();
        if(selected0.length==0){
            return res.status(404).send("User tidak terdaftar di database.");
        }
        var username=selected0[0].username;
        var nama_pemilik=selected0[0].nama_user;

        //select company
        conn = await db.getConn();
        var q1 = `select * from perusahaan where fk_user='${id}'and nama_perusahaan='${nama_perusaahan}'`;
        let selected1 = await db.executeQuery(conn, q1);
        conn.release();
        if(selected1.length==0){
            return res.status(404).send("Data Perusahaan tidak terdaftar di database.");
        }
        email_perusahaan_lama=selected1[0].email_perusahaan;
        alamat_perusahaan_lama=selected1[0].alamat_perusahaan;
        kota_lama=selected1[0].kota;
        notelp_perusahaan_lama=selected1[0].notelp_perusahaan;

         //update company
        conn = await db.getConn();
         var q2=`update perusahaan set email_perusahaan='${email_perusahaan_baru}',alamat_perusahaan='${alamat_perusahaan_baru}',kota='${kota_baru}',notelp_perusahaan='${notelp_perusahaan_baru}' where nama_perusahaan='${nama_perusaahan}'`;
         let updated = await db.executeQuery(conn, q2);
         conn.release();
         if(updated.affectedRows===0){
            return res.status(500).send('Internal server error!');
         }
        return res.status(200).json({
            username_pemilik :username,
            nama_pemilik:nama_pemilik,
            "nama":nama_perusaahan,
            previous_perusahaan: {
                "email":email_perusahaan_lama,
                "alamat":alamat_perusahaan_lama,
                "kota": kota_lama,
                "nomor_telepon":notelp_perusahaan_lama
            },
            updated_perusahaan: {
                "email":email_perusahaan_baru,
                "alamat":alamat_perusahaan_baru,
                "kota": kota_baru,
                "nomor_telepon":notelp_perusahaan_baru
            }
        });
    }
});

router.delete('/', async(req,res) => {
    //role user harus owner company (C) (x-auth-token)
    //body : nama_perusahaan
    //cek user di db, cek perusahaan di db, soft del
    // return status(200).send("Perusahaan Berhasil di hapus");

    //verify token
    var verifying = jwt.verifyToken(req.header('x-auth-token'));
    if(verifying.status!=200){
        return res.status(verifying.status).send(verifying.message);
    }
    var id=verifying.data.id;

    //cek role usernya
    if(verifying.data.role!="C"){
        return res.status(401).send("Hapus perusahaan wajib dari pemilik perusahaan(C)"); 
    }

    //cek input
    if (req.body.nama_perusahaan == undefined || req.body.nama_perusahaan == "") {return res.status(400).send("Nama Perusahaan Belum Diinputkan"); }
    else{
        var nama_perusahaan=req.body.nama_perusahaan;
        //select user
        let conn = await db.getConn();
        var q0 = `select * from users where id_user = '${id}'`;
        let selected0 = await db.executeQuery(conn, q0);
        conn.release();
        if(selected0.length==0){
            return res.status(404).send("User tidak terdaftar di database.");
        }
        //select company
        conn = await db.getConn();
        var q1 = `select * from perusahaan where fk_user='${id}' and nama_perusahaan='${nama_perusahaan}'`;
        let selected1 = await db.executeQuery(conn, q1);
        conn.release();
        if(selected1.length==0){
            return res.status(404).send("Data Perusahaan tidak terdaftar di database.");
        }
        //soft delete
        conn = await db.getConn();
        var q2 = `update perusahaan set status_perusahaan=0 where id_perusahaan='${selected1[0].id_perusahaan}'`;
        let deleted = await db.executeQuery(conn, q2);
        conn.release();
        if(deleted.affectedRows===0){ return res.status(500).send('Internal server error!');}
        return res.status(200).send("Perusahaan Berhasil di hapus");        
    }


});
module.exports = router;