const express = require('express');
const router = express.Router();
const db = require('../connection');
const jwt = require('../Authentication')

// Mailer
var nodemailer = require('nodemailer');
var mail = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: 'dothetask.service@gmail.com',
      pass: 'kovjwapavvlpqyvz'
    }
});

// FOR UPLOAD
const upload_dir = './public/uploads';
const multer = require('multer');
const fs = require('fs'); 

// FOR GOOGLE
const {google} = require('googleapis');

const checkFileType = (req, file, cb) => {
    const filetypes = /jpeg|jpg|png|gif/;
    const extname = filetypes.test(file.originalname.split('.')[file.originalname.split('.').length-1]);
    const mimetype = filetypes.test(file.mimetype);
    
    if(mimetype && extname){
        return cb(null,true);
    }
    else {
        req.msg = `Filetype salah`;
        cb(error = 'Error: filetype salah');
    }
}

const storage = multer.diskStorage({
    destination: function (req, file, callback) {
        // console.log(file);
        callback(null, upload_dir)
    },
    filename: function (req, file, callback) {
        // Ganti nama disini
        let code = "BUKTI_TRANSFER_";
        let token = req.header('x-auth-token');
        let verified = jwt.verifyToken(token);
        code += verified.data.id;

        nama = file.originalname.split(".");
        new_name = code + "." + nama[1];      
        req.body.bukti_transfer = upload_dir + "/" +  new_name;  
        
        callback(null, new_name);
    }
});

const upload = multer({ 
    storage: storage,
    fileFilter: function(req,file,cb) {
        checkFileType(req,file,cb);
    }
});

//GET/subscribe
router.post('/subscribe', upload.single("bukti_transfer") ,async(req,res) => {
    let token = req.header('x-auth-token');
    let verified = jwt.verifyToken(token);

    // Check apakah sudah upload bukti transfer
    if(req.file)
    {
        // Cek apakah verify berhasil
        if(verified.status == 200)
        {
            const conn = await db.getConn();
            q = `SELECT * FROM users WHERE id_user = ${verified.data.id}`;
            let data_user = await db.executeQuery(conn,q );
            data_user = data_user[0];
            
            // cek apakah id_user valid
            if(data_user != null)
            {
                // Jika terdaftar cek apakah user tersebut merupakan pelamar
                q = `SELECT * FROM pelamar WHERE fk_user = ${data_user.id_user} `;
                let pelamar = await db.executeQuery(conn, q);
                pelamar = pelamar[0];
                
                if(pelamar != null)
                {
                    // Melakukan pengecekan apakah pelamar sudah pernah melakukan subscription
                    q = `SELECT * FROM subscriptions WHERE fk_pelamar = ${pelamar.id_pelamar}`
                    let check_status = await db.executeQuery(conn, q);
                    check_status = check_status[0];

                    // Jika belum pernah
                    if(check_status == null)
                    {
                        q = `INSERT INTO subscriptions(fk_pelamar,bukti_transfer,status_subs) VALUES(${pelamar.id_pelamar},'${req.body.bukti_transfer}',-1)`;
                        let inserted_data = await db.executeQuery(conn, q);
                        if(inserted_data.affectedRows > 0) 
                        {
                            return res.status(200).send({
                                code: 200,
                                message: "Data anda sedang dikonfirmasi, harap tunggu.",
                                data: inserted_data
                            });
                        }
                        else {
                            return res.status(500).send({
                                code: 500,
                                message: "Server bermasalah, silahkan load ulang"
                            });
                        }
                    }
                    else {
                        return res.status(400).send({
                            code: 400,
                            message: "Subscription anda masih dalam proses"
                        }); 
                    }
                }
                else {
                    return res.status(401).send({
                        code: 401,
                        message: "Tidak dapat melakukan akses, akses khusus pelamar"
                    });
                }

                
            }
            else {

                // Jika tidak terdaftar
                return res.status(400).send({
                    code: 400,
                    message: "User tidak terdaftar"
                });
            }
            return res.send(data_user)
        }
        else {
            return res.status(400).send({
                code: 400,
                message: req.msg
            });
        }
    }
    else {
        return res.status(400).send({
            code: 400,
            message: "Wajib menyertakan bukti transfer"
        });
    }
});

// Ini seharusnya bisa jadi 1
router.get('/', async(req,res) => {
    let token = req.header('x-auth-token');
    let verified = jwt.verifyToken(token);
    if(verified.status == 200)
    {   
        
        if(verified.data.role == "A")
        {
            const conn =  await db.getConn();
            q = `SELECT u.id_user as id_user,u.username as username, u.nama_user as nama_user, s.* 
                    FROM subscriptions s 
                    JOIN pelamar p ON s.fk_pelamar = p.id_pelamar 
                    JOIN users u ON u.id_user = p.fk_user
                    WHERE s.status_subs = 1`;
            
            arr_subs = await db.executeQuery(conn, q);
            
            new_value = [];
            new_value = arr_subs.map((data) => {
                stat = "Pending";
                if(data.status_subs == 0)
                    stat = "Ditolak";
                else if(data.status_subs == 1)
                    stat = "Diterima"
                    
                return {
                    id_user: data.id_user,
                    username: data.username,
                    bukti_transfer: data.bukti_transfer,
                    status_subs: stat
                }
            });
            // return res.send(arr_subs);
            return res.status(200).send(new_value);
        }
        else {
            return res.status(401).send({
                code: 401,
                message: "Unauthorized role"
            });
        }
    }
    else {
        return res.status(401).send({
            code: 401,
            message: "Unauthorized"
        });
    }

});

//ganti payment jadi pending
router.get('/pending', async(req,res) => {
    let token = req.header('x-auth-token');
    let verified = jwt.verifyToken(token);
    
    if(verified.status == 200 )
    {
        if(verified.data.role == "A")
        {
            const conn =  await db.getConn();
            q = `SELECT u.id_user as id_user,u.username as username, u.nama_user as nama_user, s.* 
                    FROM subscriptions s 
                    JOIN pelamar p ON s.fk_pelamar = p.id_pelamar 
                    JOIN users u ON u.id_user = p.fk_user
                    WHERE s.status_subs = -1`;
            
            arr_subs = await db.executeQuery(conn, q);
            
            new_value = [];
            new_value = arr_subs.map((data) => {
                stat = "Pending";
                if(data.status_subs == 0)
                    stat = "Ditolak";
                else if(data.status_subs == 1)
                    stat = "Diterima"
                    
                return {
                    id_user: data.id_user,
                    username: data.username,
                    bukti_transfer: data.bukti_transfer,
                    status_subs: stat
                }
            });
            // return res.send(arr_subs);
            return res.status(200).send(new_value);
        }
        else {
            return res.status(401).send({
                code: 401,
                message: "Unauthorized role"
            });
        }
    }
    return res.status(verified.status).send(verified);

});

router.post('/confirm', async(req,res) => {
    let input = req.body;

    let token = req.header('x-auth-token');
    let verified = jwt.verifyToken(token);

    if(verified.status == 200)
    {
        if(verified.data.role == "A")
        {
            const conn = await db.getConn();
            // Check apakah user sudah pernah melakukan pembayaran 
            q = `SELECT * FROM subscriptions WHERE id_subs = ${input.id_subs}`;
            check_subs =  await db.executeQuery(conn, q);
            // Check apakah ada id tersebut
            if(check_subs.length > 0)
            {
                if(check_subs[0].status_subs == -1)
                {
                    q = `SELECT u.id_user as id_user ,u.email_user as email_user
                    FROM subscriptions s 
                    JOIN pelamar p ON s.fk_pelamar = p.id_pelamar
                    JOIN users u ON u.id_user = p.fk_user
                    WHERE s.id_subs = ${input.id_subs}
                    `;
                    data_user = await db.executeQuery(conn, q);
                    data_user = data_user[0];


                    // Update role user
                    q = `UPDATE users SET role = 'P' WHERE id_user = ${data_user.id_user}`;
                    updated_user  = await db.executeQuery(conn, q);

                    q = `UPDATE subscriptions SET status_subs = 1 WHERE id_subs = ${input.id_subs}`;
                    updated_subs = await db.executeQuery(conn, q);

                    if(updated_user.affectedRows > 0 && updated_subs.affectedRows > 0)
                    {
                        var mailOptions = {
                            from: 'dothetask.service@gmail.com', // Jangan lupa diganti
                            to: `${data_user.email_user}`,
                            subject: '[Subscription] Payment Success',
                            text: 'Your payment was confirmed, now you can access all premium feature.'
                        };

                        mail.sendMail(mailOptions, function(error, info){
                            if (error) {
                                console.log(error);
                            } else {
                                console.log('Email sent: ' + info.response);
                            }
                        });

                        return res.status(200).send({
                            code: 200,
                            message: "Success melakukan konfirmasi pembayaran user"
                        });
                    }
                    else {
                        return res.status(500).send({
                            code: 500,
                            message: "Terjadi kesalahan selama proses"
                        });
                    }
                }
                else {
                    return res.status(400).send({
                        code: 400,
                        message: "User ini sudah dikonfirmasi"
                    })
                }
            }
            else {
                return res.status(404).send({
                    code: 404,
                    message: "Id subscription tidak ditemukan"
                })
            }
        }   
        else {
            return res.status(401).send({
                code: 401,
                message: "Unauthorized"
            });
        }
    }
    else {
        return res.status(401).send({
            code: 401,
            message: "Unauthorized"
        });
    }
});

// Dibawah ini masih coba-coba
async function GetCalendars(auth)
{
    const calendar = google.calendar({version: 'v3', auth});
    // Do the magic
    const res = await calendar.calendars.get({calendarId:"c_classroom9f0b1767@group.calendar.google.com"});
    
    return res;
}
router.get('/coba_oauth', async(req,res) => {
    
    // Baca Credential dulu 
    let cred = fs.readFileSync('credentials.json');
    cred = JSON.parse(cred);

    // Bikin class Oauthnya
    let oAuth2Client = new google.auth.OAuth2(cred.installed.client_id,cred.installed.client_secret);
    let token = fs.readFileSync('token.json');
    token = JSON.parse(token)
    console.log("Access Token : ", token.access_token);
    
    // Set auth
    oAuth2Client.setCredentials({access_token: token.access_token});

    return_data = await GetCalendars(oAuth2Client);
    return res.send(return_data)
});

module.exports = router;