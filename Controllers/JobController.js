const express = require('express');
const router = express.Router();
const db = require('../connection');
const jwt = require('../Authentication')

/*
jobs
  `id_jobs` int(11) NOT NULL,
  `fk_perusahaan` int(11) NOT NULL,
  `fk_bidang` int(11) NOT NULL,
  `min_gaji` int(11) NOT NULL,
  `maks_gaji` int(11) NOT NULL,
  `tanggal_posting` date NOT NULL,
  `jenis_pekerjaan` varchar(10) NOT NULL COMMENT 'Full-time/Part-time',
  `kualifikasi` varchar(20) NOT NULL,
  `specialisasi` varchar(20) NOT NULL
*/

//all gaby

//done
router.get('/', async(req,res) => {
    //cek token
    var verifying = jwt.verifyToken(req.header('x-auth-token'));
    if(verifying.status!=200){
        return res.status(verifying.status).send(verifying.message);
    }
    
    //query select bidang
    if(verifying.data.role=="C"){
        //role user C >> tampilkan job yg dipunyainya
        let conn = await db.getConn();
        var qUser = `select nama_user from users where id_user='${verifying.data.id}'`;
        let selectedUser = await db.executeQuery(conn, qUser);
        conn.release();
        if(selectedUser.length==0){
            return res.status(404).send("User tidak terdaftar di database.");
        }
        conn = await db.getConn();
        var queryJob=` select j.id_jobs as id, p.nama_perusahaan as perusahaan,
            b.nama_bidang as pekerjaan, j.kualifikasi as kualifikasi, 
            j.spesialisasi as spesialisasi, CONCAT('Rp ',j.min_gaji,' - Rp ',j.maks_gaji) as gaji, 
            j.jenis_pekerjaan as jenis_pekerjaan, date_format(convert(j.tanggal_posting,char), '%d/%m/%Y') as tanggal_posting
            from users u join perusahaan p on p.fk_user = u.id_user
            join jobs j on p.id_perusahaan = j.fk_perusahaan
            join bidang b on b.id_bidang=j.fk_bidang
            where p.status_perusahaan=1 and j.status_job=1 and u.id_user='${verifying.data.id}'`;
        let selected = await db.executeQuery(conn, queryJob);
        conn.release();
        console.log(selected);
        if(selected.length>0){
            return res.status(200).json({
                pemilik_perusahaan: selectedUser[0].nama_user,
                jobs: selected
            });
        }
        else{
            return res.status(404).send("Lowongan pekerjaan tidak ditemukan");
        }
    }
    else{
        //role G,P,A,N / selain C >> tampilkan semua job
        let conn = await db.getConn();
        var queryJob=` select j.id_jobs as id, p.nama_perusahaan as perusahaan,
            b.nama_bidang as pekerjaan, j.kualifikasi as kualifikasi, 
            j.spesialisasi as spesialisasi, CONCAT('Rp ',j.min_gaji,' - Rp ',j.maks_gaji) as gaji, 
            j.jenis_pekerjaan as jenis_pekerjaan, date_format(convert(j.tanggal_posting,char), '%d/%m/%Y') as tanggal_posting
            from users u join perusahaan p on p.fk_user = u.id_user
            join jobs j on p.id_perusahaan = j.fk_perusahaan
            join bidang b on b.id_bidang=j.fk_bidang
            where p.status_perusahaan=1 and j.status_job=1`;
        let selected = await db.executeQuery(conn, queryJob);
        conn.release();
        console.log(selected);
        if(selected.length>0){
            return res.status(200).json({jobs: selected});
        }
        else{
            return res.status(404).send("Lowongan pekerjaan tidak ditemukan");
        }
    }
});

//done
router.get('/:nama_perusahaan', async(req,res) => {
    //cek token
    var verifying = jwt.verifyToken(req.header('x-auth-token'));
    if(verifying.status!=200){
        return res.status(verifying.status).send(verifying.message);
    }
    
    //query select bidang
    if(verifying.data.role=="C"){
        //role user C >> tampilkan job yg dipunyainya sesuai param,
            //dan query select user yg login 
        let conn = await db.getConn();
        var qUser = `select nama_user from users where id_user='${verifying.data.id}'`;
        let selectedUser = await db.executeQuery(conn, qUser);
        conn.release();
        if(selectedUser.length==0){
            return res.status(404).send("User tidak terdaftar di database.");
        }
        conn = await db.getConn();
        var queryJob=` select j.id_jobs as id, p.nama_perusahaan as perusahaan,
            b.nama_bidang as pekerjaan, j.kualifikasi as kualifikasi, 
            j.spesialisasi as spesialisasi, CONCAT('Rp ',j.min_gaji,' - Rp ',j.maks_gaji) as gaji, 
            j.jenis_pekerjaan as jenis_pekerjaan, date_format(convert(j.tanggal_posting,char), '%d/%m/%Y') as tanggal_posting
            from users u join perusahaan p on p.fk_user = u.id_user
            join jobs j on p.id_perusahaan = j.fk_perusahaan
            join bidang b on b.id_bidang=j.fk_bidang
            where p.status_perusahaan=1 and j.status_job=1 and u.id_user='${verifying.data.id}'`;
        let like = '%'+req.params.nama_perusahaan+'%';
        var tambahan=queryJob+` and p.nama_perusahaan like '${like}'`;
        queryJob = tambahan;
        let selected = await db.executeQuery(conn, queryJob);
        conn.release();
        console.log(selected);
        if(selected.length>0){
            return res.status(200).json({
                pemilik_perusahaan: selectedUser[0].nama_user,
                jobs: selected
            });
        }
        else{
            return res.status(404).send("Lowongan pekerjaan tidak ditemukan");
        }
    }
    else{
        //role G,P,A,N / selain C >> tampilkan semua job sesuai param
        let conn = await db.getConn();
        var queryJob=` select j.id_jobs as id, p.nama_perusahaan as perusahaan,
            b.nama_bidang as pekerjaan, j.kualifikasi as kualifikasi, 
            j.spesialisasi as spesialisasi, CONCAT('Rp ',j.min_gaji,' - Rp ',j.maks_gaji) as gaji, 
            j.jenis_pekerjaan as jenis_pekerjaan, date_format(convert(j.tanggal_posting,char), '%d/%m/%Y') as tanggal_posting
            from users u join perusahaan p on p.fk_user = u.id_user
            join jobs j on p.id_perusahaan = j.fk_perusahaan
            join bidang b on b.id_bidang=j.fk_bidang
            where p.status_perusahaan=1 and j.status_job=1`;
        let like = '%'+req.params.nama_perusahaan+'%';
        var tambahan=queryJob+` and p.nama_perusahaan like '${like}'`;
        queryJob = tambahan;
        let selected = await db.executeQuery(conn, queryJob);
        conn.release();
        console.log(selected);
        if(selected.length>0){
            return res.status(200).json({jobs: selected});
        }
        else{
            return res.status(404).send("Lowongan pekerjaan tidak ditemukan");
        }
    }    
});

//done
router.post('/add', async(req,res) => { 
    //cek token
    var verifying = jwt.verifyToken(req.header('x-auth-token'));
    if(verifying.status!=200){
        return res.status(verifying.status).send(verifying.message);
    }
    else if(verifying.data.role!="C"){
        return res.status(401).send("Role user harus sebagai Pemilik Perusahaan (C)!");
    }

    //cek input
    if (req.body.nama_perusahaan == undefined || req.body.nama_perusahaan == "") return res.status(400).send("Nama Perusahaan Belum Diinputkan"); 
    else if (req.body.nama_bidang == undefined || req.body.nama_bidang == "") return res.status(400).send("Nama Bidang Belum Diinputkan"); 
    else if (req.body.min_gaji == undefined || req.body.min_gaji == "") return res.status(400).send("Gaji Minimal Belum Diinputkan"); 
    else if (req.body.maks_gaji == undefined || req.body.maks_gaji == "") return res.status(400).send("Gaji Maksimal Belum Diinputkan"); 
    else if (req.body.jenis_pekerjaan == undefined || req.body.jenis_pekerjaan == "") return res.status(400).send("Jenis Pekerjaan Belum Diinputkan"); 
    else if (req.body.kualifikasi == undefined || req.body.kualifikasi == "") return res.status(400).send("Kualifikasi Belum Diinputkan"); 
    else if (req.body.spesialisasi == undefined || req.body.spesialisasi == "") return res.status(400).send("Spesialisasi Belum Diinputkan"); 
    
    //cek isi input: gaji (min dan maks) harus angka, jenis_pekerjaan = F(fulltime)/P(parttime)
    if(req.body.min_gaji != parseInt(req.body.min_gaji)){
        return res.status(404).send("Gaji minimal tidak valid!");
    }
    else if(req.body.maks_gaji != parseInt(req.body.maks_gaji)){
        return res.status(404).send("Gaji maksimal tidak valid!");
    }
    else if(parseInt(req.body.min_gaji) <= 1000000){
        return res.status(404).send("Gaji minimal harus lebih dari Rp 1.000.000,- ");
    }
    else if(parseInt(req.body.maks_gaji) <= parseInt(req.body.min_gaji) ){
        return res.status(404).send("Gaji maksimal harus lebih besar dari gaji minimal");
    }
    else if(req.body.jenis_pekerjaan != "F" && req.body.jenis_pekerjaan != "P"){
        return res.status(404).send("Jenis pekerjaan hanya tersedia Full-time (F) dan Part-time (P)!");
    }
    
    //cek perusahaan yg diinputkan ada di dbase
    let conn = await db.getConn();
    let query = `select id_perusahaan from perusahaan where nama_perusahaan='${req.body.nama_perusahaan}' 
        and fk_user='${verifying.data.id}'`;
    let selectPerusahaan = await db.executeQuery(conn,query);
    conn.release();
    if(selectPerusahaan.length==0){
        return res.status(404).send("Nama perusahaan tidak terdaftar sebagai perusahaan anda dalam database!");
    }

    //cek bidang yg diinputkan ada di dbase ato gak
    conn = await db.getConn();
    let queryBidang = `select id_bidang from bidang where nama_bidang='${req.body.nama_bidang}'`;
    let selectBidang = await db.executeQuery(conn,queryBidang);
    conn.release();
    if(selectBidang.length==0){
        return res.status(404).send("Bidang tidak terdaftar dalam database");
    }

    //cek jobs dari nama perusahaan dan nama bidang yg diinputkan ada di dbase
    conn = await db.getConn();
    let queryCek = `select count(*) as hasil
        from perusahaan p join jobs j on p.id_perusahaan = j.fk_perusahaan
        join bidang b on b.id_bidang=j.fk_bidang
        where p.status_perusahaan=1 and j.status_job=1 and 
        p.nama_perusahaan='${req.body.nama_perusahaan}' and b.nama_bidang='${req.body.nama_bidang}'`;
    let selected = await db.executeQuery(conn,queryCek);
    conn.release();
    if(selected[0].hasil==1){
        return res.status(400).send("Lowongan pekerjaan pada perusahaan tersebut sudah terdaftar dalam database!");
    }
        
    //masukkan jenisnya
    let showJenis = "Full-time";
    if(req.body.jenis_pekerjaan=="P") showJenis="Part-time";

    //tanggal hari ini json   
    let today = new Date();
    let tanggal = today.getDate();
    let bulan = today.getMonth()+1;
    let tahun = today.getFullYear();
    let hari_ini = tanggal+"/"+bulan+"/"+tahun;
    if(bulan<10) hari_ini = tanggal+"/0"+bulan+"/"+tahun;

    //query insert job 
    conn = await db.getConn();
    let queryNew = `insert into jobs values ('','${selectPerusahaan[0].id_perusahaan}','${selectBidang[0].id_bidang}',
        '${req.body.min_gaji}','${req.body.maks_gaji}',STR_TO_DATE('${hari_ini}', "%d/%m/%Y"),
        '${showJenis}','${req.body.kualifikasi}','${req.body.spesialisasi}',1)`;
    console.log(queryNew);
    let insertJob = await db.executeQuery(conn,queryNew);
    conn.release();
    if (insertJob.affectedRows === 0) {
        return res.status(500).send('Internal server error!');
    }
    return res.status(201).json({
        new_job: {
            perusahaan: req.body.nama_perusahaan,
            pekerjaan: req.body.nama_bidang,
            kualifikasi: req.body.kualifikasi,
            spesialisasi: req.body.spesialisasi,
            gaji: "Rp "+req.body.min_gaji+" - Rp "+req.body.maks_gaji,
            jenis_pekerjaan: showJenis,
            tanggal_posting: hari_ini
        }
    });
});

//done
router.put('/', async(req,res) => { 
    //cek token
    var verifying = jwt.verifyToken(req.header('x-auth-token'));
    if(verifying.status!=200){
        return res.status(verifying.status).send(verifying.message);
    }
    else if(verifying.data.role!="C"){
        return res.status(401).send("Role user harus sebagai Pemilik Perusahaan (C)!");
    }

    //cek input
    if (req.body.nama_perusahaan == undefined || req.body.nama_perusahaan == "") return res.status(400).send("Nama Perusahaan Belum Diinputkan"); 
    else if (req.body.nama_bidang == undefined || req.body.nama_bidang == "") return res.status(400).send("Nama Bidang Belum Diinputkan"); 
    else if (req.body.min_gaji_baru == undefined || req.body.min_gaji_baru == "") return res.status(400).send("Gaji Minimal Baru Belum Diinputkan"); 
    else if (req.body.maks_gaji_baru == undefined || req.body.maks_gaji_baru == "") return res.status(400).send("Gaji Maksimal Baru Belum Diinputkan"); 
    else if (req.body.jenis_pekerjaan_baru == undefined || req.body.jenis_pekerjaan_baru == "") return res.status(400).send("Jenis Pekerjaan Baru Belum Diinputkan"); 
    else if (req.body.kualifikasi_baru == undefined || req.body.kualifikasi_baru == "") return res.status(400).send("Kualifikasi Baru Belum Diinputkan"); 
    else if (req.body.spesialisasi_baru == undefined || req.body.spesialisasi_baru == "") return res.status(400).send("Spesialisasi Baru Belum Diinputkan"); 
    
    //cek isi input: gaji baru (min dan maks) harus angka, jenis_pekerjaan baru = F(fulltime)/P(parttime)
    if(req.body.min_gaji_baru != parseInt(req.body.min_gaji_baru)){
        return res.status(404).send("Gaji minimal harus berupa angka!");
    }
    else if(req.body.maks_gaji_baru != parseInt(req.body.maks_gaji_baru)){
        return res.status(404).send("Gaji maksimal harus berupa angka!");
    }
    else if(parseInt(req.body.min_gaji_baru) <= 1000000){
        return res.status(404).send("Gaji minimal harus lebih dari Rp 1.000.000,- ");
    }
    else if(parseInt(req.body.maks_gaji_baru) <= parseInt(req.body.min_gaji_baru) ){
        return res.status(404).send("Gaji maksimal harus lebih besar dari gaji minimal");
    }
    else if(req.body.jenis_pekerjaan_baru != "F" && req.body.jenis_pekerjaan_baru != "P"){
        return res.status(404).send("Jenis pekerjaan hanya tersedia Full-time (F) dan Part-time (P)!");
    }

    //cek jobs dari nama perusahaan dan nama bidang yg diinputkan ada di dbase
    let conn = await db.getConn();
    let query = `select j.id_jobs as id, j.kualifikasi as kualifikasi, 
        j.spesialisasi as spesialisasi, CONCAT('Rp ',j.min_gaji,' - Rp ',j.maks_gaji) as gaji, 
        j.jenis_pekerjaan as jenis_pekerjaan, date_format(convert(j.tanggal_posting,char), '%d/%m/%Y') as tanggal_posting
        from users u join perusahaan p on p.fk_user = u.id_user
        join jobs j on p.id_perusahaan = j.fk_perusahaan
        join bidang b on b.id_bidang=j.fk_bidang
        where p.status_perusahaan=1 and j.status_job=1 and u.id_user='${verifying.data.id}' 
        and p.nama_perusahaan='${req.body.nama_perusahaan}' and b.nama_bidang='${req.body.nama_bidang}'`;
    let selected = await db.executeQuery(conn,query);
    conn.release();
    if(selected.length==0){
        return res.status(404).send("Lowongan pekerjaan pada perusahaan tersebut tidak terdaftar dalam database!");
    }
    
    //masukkan jenisnya
    let showNewJenis = "Full-time";
    if(req.body.jenis_pekerjaan_baru=="P") showNewJenis="Part-time";

    //tanggal hari ini json   
    let today = new Date();
    let tanggal = today.getDate();
    let bulan = today.getMonth();
    let tahun = today.getFullYear();
    let hari_ini = tanggal+"/"+bulan+"/"+tahun;
    if(bulan<10) hari_ini = tanggal+"/0"+bulan+"/"+tahun;

    //query update job 
    conn = await db.getConn();
    let queryUpdate = `update jobs set min_gaji='${req.body.min_gaji_baru}', maks_gaji='${req.body.maks_gaji_baru}', 
        kualifikasi='${req.body.kualifikasi_baru}', jenis_pekerjaan='${showNewJenis}', spesialisasi='${req.body.spesialisasi_baru}',  
        tanggal_posting=STR_TO_DATE('${hari_ini}', "%d/%m/%Y") where id_jobs='${selected[0].id}'`;
    let updated = await db.executeQuery(conn,queryUpdate);
    conn.release();
    if(updated.affectedRows === 0){
        return res.status(500).send("Internal Server Error!");
    }
    return res.status(200).json({
        perusahaan: req.body.nama_perusahaan,
        pekerjaan: req.body.nama_bidang,
        previous_job: {
            spesialisasi: selected[0].spesialisasi,
            kualifikasi: selected[0].kualifikasi,
            gaji: selected[0].gaji,
            jenis_pekerjaan: selected[0].jenis_pekerjaan,
            tanggal_posting: selected[0].tanggal_posting
        },
        updated_job:{
            spesialisasi: req.body.spesialisasi_baru,
            kualifikasi: req.body.kualifikasi_baru,
            gaji: "Rp "+req.body.min_gaji_baru+" - Rp "+req.body.maks_gaji_baru,
            jenis_pekerjaan: showNewJenis,
            tanggal_posting_update: hari_ini
        }
    })    
}); 

//done
router.delete('/', async(req,res) => {
    //cek token
    var verifying = jwt.verifyToken(req.header('x-auth-token'));
    if(verifying.status!=200){
        return res.status(verifying.status).send(verifying.message);
    }
    else if(verifying.data.role!="C"){
        return res.status(401).send("Role user harus sebagai Pemilik Perusahaan (C)!");
    }

    //cek input
    if (req.body.nama_perusahaan == undefined || req.body.nama_perusahaan == "") return res.status(400).send("Nama Perusahaan Belum Diinputkan"); 
    else if (req.body.nama_bidang == undefined || req.body.nama_bidang == "") return res.status(400).send("Nama Bidang Belum Diinputkan"); 
    
    //cek jobs dari nama perusahaan dan nama bidang yg diinputkan ada di dbase
    let conn = await db.getConn();
    let query = `select j.id_jobs as id, j.kualifikasi as kualifikasi, 
        j.spesialisasi as spesialisasi, CONCAT('Rp ',j.min_gaji,' - Rp ',j.maks_gaji) as gaji, 
        j.jenis_pekerjaan as jenis_pekerjaan, date_format(convert(j.tanggal_posting,char), '%d/%m/%Y') as tanggal_posting
        from users u join perusahaan p on p.fk_user = u.id_user
        join jobs j on p.id_perusahaan = j.fk_perusahaan
        join bidang b on b.id_bidang=j.fk_bidang 
        where p.status_perusahaan=1 and j.status_job=1 and u.id_user='${verifying.data.id}' 
        and p.nama_perusahaan='${req.body.nama_perusahaan}' and b.nama_bidang='${req.body.nama_bidang}'`;
    let selected = await db.executeQuery(conn,query);
    conn.release();
    if(selected.length==0){
        return res.status(404).send("Lowongan pekerjaan pada perusahaan tersebut tidak terdaftar dalam database!");
    }

    //query delete job 
    conn = await db.getConn();
    let queryUpdate = `update jobs set status_job = 0 where id_jobs='${selected[0].id}'`;
    let updated = await db.executeQuery(conn,queryUpdate);
    conn.release();
    if(updated.affectedRows === 0){
        return res.status(500).send("Internal Server Error!");
    }
    return res.status(200).json({
        perusahaan: req.body.nama_perusahaan,
        pekerjaan: req.body.nama_bidang,
        spesialisasi: selected[0].spesialisasi,
        kualifikasi: selected[0].kualifikasi,
        gaji: selected[0].gaji,
        jenis_pekerjaan: selected[0].jenis_pekerjaan,
        tanggal_posting: selected[0].tanggal_posting,
        status: "Nonactive"
    });
    
});


// Rick
// FOR UPLOAD
const upload_dir = './public/applications';
const multer = require('multer');
const fs = require('fs'); 
// FOR GOOGLE
const {google} = require('googleapis');

const checkFileType = (req, file, cb) => {
    const filetypes = /doc|docs|pdf/;
    const extname = filetypes.test(file.originalname.split('.')[file.originalname.split('.').length-1]);
    const mimetype = filetypes.test(file.mimetype);
    
    if(mimetype && extname){
        return cb(null,true);
    }
    else {
        req.msg = `Filetype salah`;
        cb(error = 'Error: filetype salah');
    }
}

const storage = multer.diskStorage({
    destination: async function (req, file, callback) {
        // console.log(file);
        callback(null, upload_dir)
    },
    filename: async function (req, file, callback) {
        // Ganti nama disini
        let code = "APPLICATION_";
        const conn = await db.getConn();

        q = `SELECT MAX(id_app) as id_app FROM applications`;
        id = await db.executeQuery(conn, q);
        if(id == null)
        {   
            code += 1;
        }
        else {
            code += id[0].id_app + 1;
        }

        nama = file.originalname.split(".");
        new_name = code + "." + nama[1];      
        req.body.file_cv = upload_dir + "/" +  new_name;  
        conn.release();
        
        callback(null, new_name);
    }
});

const upload = multer({ 
    storage: storage,
    fileFilter: function(req,file,cb) {
        checkFileType(req,file,cb);
    }
});

router.post('/apply', upload.single("file_cv"), async(req,res) => {
    let token = req.header('x-auth-token');
    let verified = jwt.verifyToken(token);

    // Check apakah file CV nya ada atau tidak 
    if(req.file)
    {
        // Verifying JWT
        if(verified.status == 200)
        {
            // Check role
            if(verified.data.role == "P" || verified.data.role == "N")
            {
                const conn = await db.getConn();
                // Get Data pelamar 
                q = `SELECT * FROM pelamar WHERE fk_user = ${verified.data.id}`
                data_pelamar = await db.executeQuery(conn, q);
                data_pelamar = data_pelamar[0];
                if(data_pelamar != null)
                {
                    current_date = new Date();
                    submitted_at = current_date.getFullYear() + "-" + (current_date.getMonth()+1) + "-" + current_date.getDate();
    
                    //SELECT DATE_FORMAT(submitted_at,'%Y-%m-%d') FROM applications WHERE DATE_FORMAT(submitted_at,'%Y-%m-%d') = "2021-05-19"
                    q = `SELECT * FROM applications WHERE DATE_FORMAT(submitted_at, '%Y-%m-%d') = STR_TO_DATE('${submitted_at}','%Y-%m-%d') AND fk_pelamar  = ${data_pelamar.id_pelamar}`;
                    check_submit = await db.executeQuery(conn,q);
    
                    // Melakukan pengecekan untuk user yang normal
                    let can_continue = true;
                    if(verified.data.role == "N" && check_submit.length >= 2) can_continue = false;
    
                    if(can_continue)
                    {
                        // Melakukan pengecekan apakah job ada dan dalam status menerima pelamar
                        q = `SELECT * FROM jobs WHERE id_jobs = ${req.body.id_jobs}`;
                        check_jobs = await db.executeQuery(conn, q);
                        
                        if(check_jobs.length > 0)
                        {
                            if(check_jobs[0].status_job == 1)
                            {
                                q = `SELECT * FROM applications WHERE fk_jobs = ${req.body.id_jobs} AND fk_pelamar = ${data_pelamar.id_pelamar}`;
                                check_done = await db.executeQuery(conn, q);
                                
                                // Belum pernah mengirim CV sebelumnya
                                if(check_done.length == 0)
                                {
                                    q = `INSERT INTO applications(fk_jobs,fk_pelamar,file_cv,status_app,submitted_at) VALUES(${req.body.id_jobs},${data_pelamar.id_pelamar},'${req.body.file_cv}',-1,'${submitted_at}')`;
                                    inserted_data = await db.executeQuery(conn,q);
                                    
                                    if(inserted_data.affectedRows > 0)
                                    {
                                        return res.status(201).send({
                                            code: 201,
                                            message: "Success mengirim lamaran"
                                        });
                                    }   
                                    else {
                                        return res.status(500).send({
                                            code: 500,
                                            message: "Terjadi error pada server"
                                        })
                                    }
                                }
                                else {
                                    return res.status(400).send({
                                        code: 400,
                                        message: "Sudah pernah mengirim CV sebelumnya"
                                    });
                                }
                            }
                            else {
    
                                return res.status(400).send({
                                    code : 400,
                                    message : "Tidak dapat melamar pada job ini"
                                });
                            }
                        }
                        else {
                            return res.status(404).send({
                                code: 404,
                                message: "Job tidak ditemukan"
                            })
                        }
                    }
                    else {
                        return res.status(400).send({
                            code: 400,
                            message: "Tidak dapat submit, hari ini anda sudah submit 2 kali"
                        });
                    }
                }
            }
            else {
                return res.status(401).send({
                    code: 401,
                    message: "Role tidak sesuai"
                });
            }
        }
        else {
            return res.status(401).send({
                code: 401,
                message: "Unauthorized"
            })
        }
    }
    else{ 
        return res.send({
            code: 400,
            message: "Harus ada file"
        });
    }
});

module.exports = router;

