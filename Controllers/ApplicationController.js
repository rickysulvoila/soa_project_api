const express = require('express');
const router = express.Router();
const db = require('../connection');
const jwt = require('../Authentication');

// Filesystem
const fs = require('fs'); 

// FOR GOOGLE
const {google} = require('googleapis');
const CALENDAR_ID = `c_m25o75u49quhvv8mvrgnn3u1vk@group.calendar.google.com`;

//GET/application/:nama_perusahaan
router.get('/getApplicant/:id_perusahaan', async(req,res) => {
    let token = req.header('x-auth-token');
    let verified = jwt.verifyToken(token);

    if(verified.status == 200)
    {
        const conn = await db.getConn();
        // Check if user login = pemilik perusahaan
        q = `SELECT * FROM perusahaan WHERE fk_user = ${verified.data.id} AND id_perusahaan = ${req.params.id_perusahaan}`;
        check_cred = await db.executeQuery(conn, q);
        if(check_cred.length > 0)
        {
            if(req.params.id_perusahaan != null)
            {
                q = `SELECT id_jobs,b.nama_bidang 
                FROM jobs j JOIN bidang b ON j.fk_bidang = b.id_bidang 
                WHERE fk_perusahaan = ${req.params.id_perusahaan}`;
                
                list_job = await db.executeQuery(conn, q);

                applicants = []
                for(let i = 0 ; i < list_job.length ; i++)
                {
                    q = `SELECT u.nama_user, a.id_app, a.file_cv, "Menunggu Konfirmasi" as status_app, a.submitted_at
                    FROM applications a
                    JOIN pelamar p ON p.id_pelamar = a.fk_pelamar 
                    JOIN users u ON p.fk_user = u.id_user  
                    WHERE a.fk_jobs = ${list_job[i].id_jobs} AND a.status_app = -1`;

                    arr_applicants = await db.executeQuery(conn, q);
                    applicants.push(arr_applicants);                
                    list_job[i].applicants = applicants[i];
                }

                return res.status(200).send({
                    code: 200,
                    data: list_job
                })
            }
            else {
                return res.status(404).send({
                    code: 404,
                    message: "Request tidak dapat dilakukan"
                });
            }
        }
        else {
            return res.status(401).send({
                code: 401,
                message: "Anda bukan pemilik perusahaan ini"
            })
        }
        
    }
    else {
        return res.status(401).send(verified);
    }
        
});

//GET/application/history/:nama_perusahaan
router.get('/history/get', async(req,res) => {
    let token = req.header('x-auth-token');
    let verified = jwt.verifyToken(token);

    if(verified.status == 200)
    {
        if(verified.data.role == "N" || verified.data.role == "P")
        {       
            const conn = await db.getConn();
            q = `SELECT a.id_app as id_app, a.fk_jobs as id_jobs, a.file_cv, per.nama_perusahaan as nama_perusahaan,
            CASE 
                WHEN status_app = -1 THEN "Menunggu konfirmasi"
                WHEN status_app = 0 THEN "Ditolak"
                WHEN status_app = 1 THEN "Diterima"
            END as status_app
            FROM users u 
            JOIN pelamar p ON p.fk_user = u.id_user 
            JOIN applications a ON a.fk_pelamar = p.id_pelamar
            JOIN jobs j ON j.id_jobs = a.fk_jobs
            JOIN perusahaan per ON per.id_perusahaan = j.fk_perusahaan 
            WHERE u.id_user = ${verified.data.id}`;

            coba = await db.executeQuery(conn, q);
            return res.send(coba);
        }
        else {
            return res.status(401).send({
                code: 401,
                message: "Hanya untuk pelamar"
            });
        }
    }
    else {
        return res.status(401).send(verified);
    }
});

async function GetCalendars(auth)
{
    const calendar = google.calendar({version: 'v3', auth});
    // Do the magic
    const res = await calendar.calendars.get({calendarId:"c_classroom9f0b1767@group.calendar.google.com"});
    
    return res;
}

async function GetCalendarsList(auth)
{
    const calendar = google.calendar({version: 'v3', auth});
    // Do the magic
    const res = await calendar.calendarList.list({
        maxResults: 200
    });
    
    return res;
}

var moment = require('moment-timezone');
moment().tz("Asia/Jakarta").format();

async function createEvent(auth, data) {
    const calendar = google.calendar({version: 'v3', auth});
    temp_start = moment(data.start_meet, "DD/MM/YYYY HH:mm:ss");
    start_time = temp_start.utc().format();
    
    temp_end = moment(data.end_meet, "DD/MM/YYYY HH:mm:ss");
    end_time = temp_end.utc().format();

    var event = {
        'summary': data.summary_meet,
        'location': data.location_meet,
        'description': data.description_meet,
        'start': {
          'dateTime': start_time,
          'timeZone': "Asia/Jakarta",
        },
        'end': {
          'dateTime': end_time,
          'timeZone': "Asia/Jakarta",
        },
        'attendees': [
          {'email': 'lpage@example.com'},
          {'email': 'sbrin@example.com'},
        ]
    };

    const res = await calendar.events.insert({
        calendarId: CALENDAR_ID,
        requestBody: event
    });

    return res;
}

// Ini menunggu create event google calendar
router.post('/confirm', async(req,res) => {
    let token = req.header('x-auth-token');
    let verified = jwt.verifyToken(token);
    if(verified.status == 200)
    {
        const conn = await db.getConn();
        // Check id app 
        q = `SELECT * FROM applications WHERE id_app = ${req.body.id_app}`;
        check_app = await db.executeQuery(conn ,q);

        if(check_app.length > 0)
        {
            if(check_app[0].status_app == -1)
            {
                // Check user 
                q = `SELECT users.* 
                FROM applications app 
                JOIN jobs ON app.fk_jobs = jobs.id_jobs
                JOIN perusahaan ON perusahaan.id_perusahaan = jobs.fk_perusahaan
                JOIN users ON perusahaan.fk_user = users.id_user 
                WHERE users.id_user = ${verified.data.id} AND app.id_app = ${req.body.id_app}`;

                check_cred = await db.executeQuery(conn, q);
                if(check_cred.length > 0)
                {
                    if(req.body.status_app == 1)
                    {
                        // return res.send(req.body)

                        // Baca Credential dulu 
                        let cred = fs.readFileSync('credentials.json');
                        cred = JSON.parse(cred);

                        // Bikin class Oauthnya
                        let oAuth2Client = new google.auth.OAuth2(cred.installed.client_id,cred.installed.client_secret,[
                            "https://www.googleapis.com/auth/calendar",
                            "https://www.googleapis.com/auth/calendar.events",
                        ]);
                        
                        let token = fs.readFileSync('token.json');
                        token = JSON.parse(token)
                        console.log("Access Token : ", token.access_token);

                        // Set auth
                        oAuth2Client.setCredentials({access_token: token.access_token});

                        return_data = await createEvent(oAuth2Client,req.body);
                        if(return_data.status == 200)
                        {
                            q = `UPDATE applications SET status_app = 1, event_id = '${return_data.data.id}' WHERE id=${req.body.id_app}`;
                            updated_user = await db.executeQuery(conn, q);
                            return res.status(200).send({
                                code: 200,
                                data: return_data.config
                            })
                        }
                        else {
                            return res.status(500).send({
                                code: 500,
                                message: "Terjadi kesalahan pada server"
                            });
                        }
                    }
                    else if(req.body.status_app == 0)
                    {
                        // Pelamar ditolak   
                        q = `UPDATE applications SET status_app = 0 WHERE id_app = ${req.body.id_app}`;
                        updated_user = await db.executeQuery(conn, q);

                        if(updated_user.affectedRows > 0)
                        {   
                            return res.status(200).send({
                                code: 200,
                                message: "Success menolak applicant"
                            });
                        }
                        else {
                            return res.status(500).send({
                                code: 500,
                                message: "Terjadi kesalahan pada server"
                            });
                        }
                    }
                    else {
                        return res.status(400).send({
                            code :400,
                            message: "Field status tidak valid"
                        })
                    }
                }
                else {
                    return res.status(401).send({
                        code: 401,
                        message: "Unauthorized"
                    })
                }
            }
            else {
                return res.status(400).send({
                    code: 400,
                    message: "Sudah melakukan konfirmasi terhadap applicant ini"
                });
            }
            
        }
        else {
            return res.status(404).send({
                code: 404,
                message: "ID Application tidak ditemukan"
            });
        }
    }
    else {
        return res.status(401).send(verified);
    }
        
});
module.exports = router;
