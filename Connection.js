const mysql = require('mysql');
const config = {
    host: "localhost",
    database: "soa_project", // Ganti nama database disini
    user: "root",
    password: ""
};
const conn = mysql.createPool(config);

exports.getConn = () => {
    return new Promise( (resolve, reject) => {
        conn.getConnection((err, connection) => {
            if(err)
            {
                reject(err);
            }
            else {
                resolve(connection);
            }
        });
    });
}
// For insert purpose
exports.executeQueryWithParam = (conn, q, data) => {
    return new Promise( (resolve, reject) => {
        // Data disini parameter yang dilemparkan
        conn.query(q,data,(err, result) => {
            if(err)
            {
                reject(err);
            }
            else {
                resolve(result);
            }
        });
    });
}

// For select purpose
exports.executeQuery = (conn, q) => {
    return new Promise( (resolve, reject) => {
        // Data disini parameter yang dilemparkan
        conn.query(q, (err, result) => {
            if(err)
            {
                reject(err);
            }
            else {
                resolve(result);
            }
        });
    });
}

exports.getConfig = config;